package main.chat.interactor

interface ChatInteractor {

    interface ConsumerFromQueue {
        fun onNewMessageFromQueue(message: String)

        fun onNewMessageFromExchage(message: String)
    }

    fun startConsumerFromQueue(queueName: String, consumer: ConsumerFromQueue)

    fun createChannel()

    fun closeConnection()

    fun createExchange(exchangeName: String)

    fun sendMessageExchange(exchangeName: String, message: String): Boolean

    fun createQueue(name: String): Boolean

    fun sendMessageToQueue(queueName: String, message: String): Boolean

    fun addQueueToExchange(queueName: String, exchangeName: String): Boolean

    fun removeQueueToExchange(queueName: String, exchangeName: String): Boolean
}