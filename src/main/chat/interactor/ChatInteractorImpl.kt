package main.chat.interactor

import com.rabbitmq.client.*
import main.chat.utils.toMensagem
import java.io.IOException
import java.nio.charset.Charset

/**
 * @author Givaldo Marques dos Santos - 201422029045
 * 08 de setembro de 2017
 */

class ChatInteractorImpl : ChatInteractor {
    private var connection: Connection? = null
    private var channel: Channel? = null

    /**
     * cria um canal no rabbitmq
     */
    override fun createChannel() {
        val factory = ConnectionFactory()
        factory.host = "black-boar.rmq.cloudamqp.com"
        factory.username = "oybfghsn"
        factory.virtualHost = "oybfghsn"
        factory.password = "gGHltUZQw6eQUBLvvxV3zg-YK5RrYjLu"

        connection = factory.newConnection()
        channel = connection?.createChannel()
    }

    /**
     * encerra a conexão com o rabbitmq
     */
    override fun closeConnection() {
        channel?.close()
        connection?.close()
    }

    /**
     * Cria um grupo do tipo fanout (todos inscritos recebem) (exchange == grupo)
     * @param exchangeName nome do grupo
     */
    override fun createExchange(exchangeName: String) {
        try {
            channel?.exchangeDeclare(exchangeName, BuiltinExchangeType.FANOUT)
        } catch (e: AlreadyClosedException) {

        }
    }

    /**
     * envia mensagem para um grupo selecionado (exchange == grupo)
     * @param exchangeName nome do grupo
     * @param message mensagem a ser enviada
     */

    override fun sendMessageExchange(exchangeName: String, message: String): Boolean {
        try {
            channel?.basicPublish(exchangeName, "", null, message.toByteArray()) ?: return false
        } catch (e: AlreadyClosedException) {
            return false
        }
        return true

    }

    /**
     * Cria uma fila no canal atual (queue == usuário)
     * @param name nome da fila a ser criada
     * @return true se pode criar, false se houve um erro
     */
    override fun createQueue(name: String): Boolean {
        try {
            channel?.queueDeclare(name, true, false, false, null) ?: return false
        } catch (e: IOException) {
            return false
        }
        return true

    }

    /**
     * envia uma mensagem para uma fila selecinada (fila == usuário)
     * @param queueName nome da fila
     * @param message mensagem a ser enviada
     */
    override fun sendMessageToQueue(queueName: String, message: String): Boolean {
        try {
            channel?.basicPublish("", queueName, null, message.toByteArray()) ?: return false
        } catch (e: IOException) {
            return false
        }
        return true

    }

    /**
     * adiciona usuário ao grupo
     * @param queueName nome do usuário a ser adicionado
     * @param exchangeName nome do grupo a adicionar o usuário
     * @return true se adicionou, false se houve algum erro
     */
    override fun addQueueToExchange(queueName: String, exchangeName: String): Boolean {
        try {
            channel?.queueBind(queueName, exchangeName, "") ?: return false
        } catch (e: IOException) {
            return false
        }
        return true

    }

    /**
     * remove usuário ao grupo
     * @param queueName nome do usuário a ser removico
     * @param exchangeName nome do grupo a remover o usuário
     * @return true se adicionou, false se houve algum erro
     */
    override fun removeQueueToExchange(queueName: String, exchangeName: String): Boolean {
        try {
            channel?.queueUnbind(queueName, exchangeName, "") ?: return false
        } catch (e: IOException) {
            return false
        }
        return true

    }

    /**
     * remove exchange (um grupo)
     * @param exchangeName nome do grupo a ser removido
     * @return true se remover, false se houve algum erro
     */
    fun deleteExchange(exchangeName: String): Boolean {
        try {
            channel?.exchangeDelete(exchangeName) ?: return false
        } catch (e: IOException) {
            return false
        }
        return true
    }


    override fun startConsumerFromQueue(queueName: String, consumer: ChatInteractor.ConsumerFromQueue) {
        val c = object : DefaultConsumer(channel) {
            override fun handleDelivery(consumerTag: String?, envelope: Envelope?, properties: AMQP.BasicProperties?, body: ByteArray?) {
                val m = body?.toString(Charset.defaultCharset())?.toMensagem()
                if (m?.group == null) {
                    consumer.onNewMessageFromQueue(m.toString())
                } else {
                    consumer.onNewMessageFromExchage(m.toString())

                }
            }
        }

        channel?.basicConsume(queueName, true, c)
    }
}