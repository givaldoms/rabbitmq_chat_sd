package main.chat

import main.chat.presenter.Chat

/**
 * @author Givaldo Marques dos Santos - 201422029045
 * 08 de setembro de 2017
 */

/*
   @joao -> proximas mensagens serão enviadas para joão
   !addGroup ufs -> cria um grupo chamado "ufs"
   !addUser joao ufs -> adiciona joão no grupo "ufs"
   #ufs -> proximas mensagens serão enviadas para o grupo "ufs"
   !delUser joao ufs-> remove joao do grupo "ufs"
   !delGroup ufs -> apaga o grupo "ufs"
    */

/**
 * Main método
 */
fun main(args: Array<String>) {
    val main = Chat()
    main.startChat()
}