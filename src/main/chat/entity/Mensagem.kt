package main.chat.entity

import com.google.gson.Gson
import main.chat.utils.getDate
import main.chat.utils.getTime
import java.nio.charset.Charset

data class Mensagem(val sender: String,
                    private val date: String = getDate(),
                    private val time: String = getTime(),
                    val group: String? = null,
                    val message: Conteudo) {

    class Conteudo(val type: String = "text/plain", val body: ByteArray, val name: String? = null)

    fun toJson() = Gson().toJson(this) ?: ""

    override fun toString(): String {
        return "($date às $time) $sender diz: ${message.body.toString(Charset.defaultCharset())}"
    }

    constructor() : this(sender = "", message = Conteudo(body = "".toByteArray()))

}



