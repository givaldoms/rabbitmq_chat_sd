package main.chat.utils

import com.google.gson.Gson
import main.chat.entity.Mensagem
import java.text.SimpleDateFormat
import java.util.*

/**
 * @return a data atual no formato dd/mm/aaaa
 */
fun getDate(): String {
    val sdf = SimpleDateFormat("dd/MM/yyyy")
    return sdf.format(Date())
}

/**
 * @return a hora atual no formato hh:mm
 */
fun getTime(): String {
    val sdf = SimpleDateFormat("hh:mm")
    return sdf.format(Date())
}

/**
 * extensão da classe "String"
 * transforma um objeto de tipo string em um objeto do tipo Mensagem
 * necessário quando recebe a mensagem do servidor
 */
fun String.toMensagem() = Gson().fromJson(this, Mensagem::class.java) ?: Mensagem()