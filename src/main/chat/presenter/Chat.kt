package main.chat.presenter

import main.chat.entity.Mensagem
import main.chat.interactor.ChatInteractor
import main.chat.interactor.ChatInteractorImpl

/**
 * @author Givaldo Marques dos Santos - 201422029045
 * 08 de setembro de 2017
 */

class Chat : ChatInteractor.ConsumerFromQueue {

    //nome do usuário ou nome do grupo que vai receber a proxima mensagem
    private var currentReceiver: String = ""

    /**
     * função disparada pelo interactor quando uma mensagem de um grupo
     * for recebida
     */
    override fun onNewMessageFromExchage(message: String) {
        print("\n$message\n$currentReceiver>>")
    }

    /**
     * função disparada pelo interactor quando uma mensagem de um usuário
     * for recebida
     */
    override fun onNewMessageFromQueue(message: String) {
        print("\n$message\n$currentReceiver>>")
    }

    /**
     * deve fornecer o nome de usuário assim que iniciar a aplicação
     * não pode ser vazio
     */
    private fun getUsername(): String {
        var username: String

        do {
            print("User: ")
            username = readLine().toString()
        } while (username.isEmpty())//validando nome do usuário (não pode ser vazio)

        return username
    }


    /**
     * o usuário deve digitar uma função ao programa
     * não pode ser vazio
     */
    private fun getLine(): String {
        var cmd: String

        do {
            print("$currentReceiver>>")
            cmd = readLine().toString()
        } while (cmd.isEmpty())

        return cmd
    }

    fun startChat() {
        val emit = ChatInteractorImpl()
        emit.createChannel()

        val username = getUsername()

        emit.createQueue(username)//criando uma fila com o nome do usuário
        emit.startConsumerFromQueue(username, this)//começa a ler a fila desse usuário

        while (true) {
            val line = getLine()

            when (line[0]) {
                '@' -> {//enviar mensagem para usuário
                    currentReceiver = line.substring(1, line.length)
                }

                '#' -> {//enviar mensagem para grupo
                    val ls = line.split(" ", limit = 2)
                    if (ls.size == 1) {//#ufs (deve ter tamanho == 1)
                        val groupName = ls[0].substring(1, ls[0].length)//remove "#" de "#ufs"
                        currentReceiver = "$groupName*"

                    }
                }

                '!' -> {
                    when (line.split(' ')[0]) {
                        "!addGroup" -> {
                            val ls = line.split(" ")
                            if (ls.size == 2) {//!addGroup ufs (deve ter tamanho 2)
                                val groupName = ls[1]
                                emit.createExchange(groupName)
                                emit.addQueueToExchange(username, groupName)
                            }
                        }

                        "!addUser" -> {
                            val ls = line.split(" ")
                            if (ls.size == 3) {//!addUser joao ufs (deve ter tamanho 3)
                                val user = ls[1]
                                val group = ls[2]
                                emit.addQueueToExchange(user, group)
                            }
                        }

                        "!delUser" -> {
                            val ls = line.split(" ")
                            if (ls.size == 3) {//!delUser joao ufs (deve ter tamanho 3)
                                val user = ls[1]
                                val group = ls[2]
                                emit.removeQueueToExchange(user, group)
                            }
                        }

                        "!delGroup" -> {
                            val ls = line.split(" ")
                            if (ls.size == 2) {//!delGroup ufs (deve ter tamaho 2)
                                val group = ls[1]
                                emit.deleteExchange(group)
                                if (currentReceiver == "$ls*") {
                                    currentReceiver = ""
                                }
                            }
                        }

                    }

                }

                else -> {
                    if (currentReceiver.isNotEmpty()) {

                        if (currentReceiver.last() == '*') {//enviar para um grupo
                            val groupName = currentReceiver.substring(0, currentReceiver.length - 1)
                            val c = Mensagem.Conteudo(body = line.toByteArray())
                            val message = Mensagem(sender = "$groupName/$username", message = c)
                            emit.sendMessageExchange(groupName, message = message.toJson())

                        } else {//enviar para um usuário
                            val c = Mensagem.Conteudo(body = line.toByteArray())
                            val message = Mensagem(sender = username, message = c)
                            emit.sendMessageToQueue(currentReceiver, message = message.toJson())
                        }
                    }
                }


            }

        }
    }

}

